package utils

import org.joda.time.format.{DateTimeFormat, DateTimeFormatter}
import org.joda.time.{DateTimeZone, LocalDate, LocalTime}
import play.api.libs.json._

import scala.reflect.runtime.universe.{TypeTag, typeOf}
import scala.util.Try

object DateFormatter {

  def now(): DateTime = org.joda.time.DateTime.now()

  // -----------------------
  //          TYPES
  // -----------------------

  // TODO move to package object
  type DateTime = org.joda.time.DateTime
  type Time = org.joda.time.LocalTime
  type Date = org.joda.time.LocalDate

  // -----------------------
  //      OWN FORMATTER
  // -----------------------

  trait DatePrinter {
    protected def vendorFormatters: Seq[DateTimeFormatter]
    def print(date: Date): String = vendorFormatters.head.print(date)
    def print(time: Time): String = vendorFormatters.head.print(time)
    def print(dateTime: DateTime): String = vendorFormatters.head.print(dateTime)
  }

  trait DateParser {

    private def tryFormatOrException[T: TypeTag](f: DateTimeFormatter => T) = {
      val parser = vendorFormatters
        .dropWhile(formatter => Try(f(formatter)).toOption.isEmpty)
        .headOption

        parser.fold(throw new IllegalArgumentException(s"Invalid format for ${typeOf[T].typeSymbol.name}"))(
          f
        )
    }

    protected def vendorFormatters: Seq[DateTimeFormatter]
    def parseDate(date: String): Date = tryFormatOrException(_.parseLocalDate(date))
    def parseTime(time: String): Time = tryFormatOrException(_.parseLocalTime(time))
    def parseDateTime(dateTime: String): DateTime = tryFormatOrException(_.withOffsetParsed().parseDateTime(dateTime))
  }

  class DateFormatter(patterns: Seq[String],
                      maybeZone: Option[String] = None) extends DateParser with DatePrinter {

    protected def vendorFormatters: Seq[DateTimeFormatter] = {
      val formatters = patterns.map(DateTimeFormat.forPattern)
      maybeZone.fold(formatters)(zone => formatters.map(_.withZone(DateTimeZone.forID(zone))))
    }
  }


  // -----------------------
  //    DEFAULTS PATTERNS
  // -----------------------

  val DefaultDatePattern: String = "yyyy-MM-dd"
  val DefaultTimePattern: String = "HH:mm:ss"
  val DefaultDateTimePattern: Seq[String] = Seq(
//    "yyyy-MM-dd'T'HH:mm:ssZ",
//    "yyyy-MM-dd'T'HH:mm:ss",
//    "yyyy-MM-dd'T'HH:mm:ss.SSS",
//    "yyyy-MM-dd HH:mm:ss.SSSSSSZ",
//    "yyyy-MM-dd'T'HH:mm:ss.SSSSSSSZ",
    "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
  )

  // -----------------------
  //  DEFAULTS JSON FORMATS
  // -----------------------

  implicit val defaultDateJsonFormat: Format[Date] = dateJsonFormat(DefaultDatePattern)
  implicit val defaultTimeJsonFormat: Format[Time] = timeJsonFormat(DefaultTimePattern)
  implicit val defaultDateTimeJsonFormat: Format[DateTime] = dateTimeJsonFormat(DefaultDateTimePattern)

  // -----------------------
  //   DEFAULTS FORMATTER
  // -----------------------

  val defaultDateFormatter = new DateFormatter(List(DefaultDatePattern))
  val defaultTimeFormatter = new DateFormatter(List(DefaultTimePattern))
  val defaultDateTimeFormatter = new DateFormatter(DefaultDateTimePattern)

  // -----------------------
  //  JSON READS BY PATTERN
  // -----------------------

  def dateJsonReads(pattern: String, zone: Option[String] = None): Reads[Date] = {
    val formatter = new DateFormatter(List(pattern), zone)
    Reads {
      case JsString(value) if value.nonEmpty => JsSuccess(formatter.parseDate(value))
      case JsNumber(value) => JsSuccess(new LocalDate(value.toLongExact))
      case invalidDate => parserError("Date", invalidDate)
    }
  }

  def timeJsonReads(pattern: String, zone: Option[String] = None): Reads[Time] = {
    val formatter = new DateFormatter(List(pattern), zone)
    Reads {
      case JsString(value) if value.nonEmpty => JsSuccess(formatter.parseTime(value))
      case JsNumber(value) => JsSuccess(new LocalTime(value.toLongExact))
      case invalidTime => parserError("Time", invalidTime)
    }
  }

  def dateTimeJsonReads(patterns: Seq[String], zone: Option[String] = None): Reads[DateTime] = {
    val formatter = new DateFormatter(patterns, zone)
    Reads {
      case JsString(value) if value.nonEmpty => JsSuccess(formatter.parseDateTime(value))
      case JsNumber(value) => JsSuccess(new DateTime(value.toLongExact))
      case invalidDateTime => parserError("DateTime", invalidDateTime)
    }
  }

  // -----------------------
  //  JSON WRITES BY PATTERN
  // -----------------------

  def dateJsonWrites(pattern: String, zone: Option[String]): Writes[Date] = {
    val formatter = new DateFormatter(List(pattern), zone)
    Writes[Date] { date => JsString(formatter.print(date)) }
  }

  def timeJsonWrites(pattern: String, zone: Option[String]): Writes[Time] = {
    val formatter = new DateFormatter(List(pattern), zone)
    Writes[Time] { time => JsString(formatter.print(time)) }
  }

  def dateTimeJsonWrites(pattern: String, zone: Option[String]): Writes[DateTime] = {
    val formatter = new DateFormatter(List(pattern), zone)
    Writes[DateTime] { dateTime => JsString(formatter.print(dateTime)) }
  }

  // -----------------------
  //  JSON FORMAT BY PATTERN
  // -----------------------

  def dateJsonFormat(pattern: String, zone: Option[String] = None): Format[Date] =
    Format(dateJsonReads(pattern), dateJsonWrites(pattern, zone))

  def timeJsonFormat(pattern: String, zone: Option[String] = None): Format[Time] =
    Format(timeJsonReads(pattern), timeJsonWrites(pattern, zone))

  def dateTimeJsonFormat(patterns: Seq[String], zone: Option[String] = None): Format[DateTime] =
    Format(dateTimeJsonReads(patterns), dateTimeJsonWrites(patterns.head, zone))

  // -----------------------
  // DIRECT FORMAT FUNCTIONS
  // -----------------------

  def serializeDate(date: Date, format: Option[String] = None): String =
    format.map(f => new DateFormatter(List(f), None)).getOrElse(defaultDateFormatter).print(date)

  def serializeTime(time: Time, format: Option[String] = None): String =
    format.map(f => new DateFormatter(List(f), None)).getOrElse(defaultTimeFormatter).print(time)

  def serializeDateTime(dateTime: DateTime, format: Option[String] = None): String =
    format.map(f => new DateFormatter(List(f), None)).getOrElse(defaultDateTimeFormatter).print(dateTime)

  def deserializeDate(date: String, format: Option[String] = None): Date =
    format.map(f => new DateFormatter(List(f), None)).getOrElse(defaultDateFormatter).parseDate(date)

  def deserializeTime(time: String, format: Option[String] = None): Time =
    format.map(f => new DateFormatter(List(f), None)).getOrElse(defaultTimeFormatter).parseTime(time)

  def deserializeDateTime(dateTime: String, format: Option[String] = None): DateTime =
    format.map(f => new DateFormatter(List(f), None)).getOrElse(defaultDateTimeFormatter).parseDateTime(dateTime)

  // -----------------------
  //    PRIVATE UTILITIES
  // -----------------------

  private def parserError(kind: String, invalidValue: Any): JsError =
    JsError(s"Error while trying to format $kind with value $invalidValue")


  // -----------------------
  //    ORDERING UTILITIES
  // -----------------------

  /** Returns an integer whose sign communicates how x compares to y.
   *
   * The result sign has the following meaning:
   *
   *  - negative if x < y
   *  - positive if x > y
   *  - zero otherwise (if x == y)
   */
  implicit val dateTimeOrdering: Ordering[DateTime] = Ordering.by(_.getMillis)

  implicit val dateOrdering: Ordering[Date] = Ordering.by(_.toDateTime(LocalTime.now()))

  implicit val timeOrdering: Ordering[Time] = Ordering.by(_.toDateTimeToday)
}


