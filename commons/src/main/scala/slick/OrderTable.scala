package slick

import models.Order
import slick.jdbc.PostgresProfile.api._
import slick.lifted.Tag
import utils.DateFormatter.DateTime

class OrderTable(tag: Tag) extends Table[Order](tag, Some("ifood"), "orders") {

  def id = column[String]("id", O.PrimaryKey)
  def customer_id = column[String]("customer_id")
  def totalValue = column[Double]("total_value")
  def createdAt = column[DateTime]("created_at")

  def * = (
    id, customer_id, totalValue, createdAt
  ) <> ((Order.apply _).tupled, Order.unapply)

}
