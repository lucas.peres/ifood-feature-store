import java.sql.Timestamp

import slick.jdbc.PostgresProfile.api._
import utils.DateFormatter.DateTime

package object slick {

  implicit def jodaTimeMapping: BaseColumnType[DateTime] = MappedColumnType.base[DateTime, Timestamp](
    dateTime => new Timestamp(dateTime.getMillis),
    timeStamp => new DateTime(timeStamp.getTime)
  )

}
