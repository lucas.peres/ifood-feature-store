package models

import play.api.libs.json._
import utils.DateFormatter._

case class Status(created_at: DateTime, order_id: String, status_id: String, value: String)

object Status {
  implicit val format: Format[Status] = Json.format

  val CONCLUDED: String = "CONCLUDED"
  val REGISTERED: String = "REGISTERED"
  val CANCELLED: String = "CANCELLED"
  val PLACED: String = "PLACED"
}