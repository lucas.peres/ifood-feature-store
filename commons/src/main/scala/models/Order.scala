package models

import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.libs.json.Reads._
import utils.DateFormatter._

case class Order(id: String, customer_id: String, totalValue: Double, createdAt: DateTime)

object Order {
  implicit val format: Format[Order] = Json.format

  val reads: Reads[Order] = (
    (__ \ "order_id").read[String] and
    (__ \ "cpf").read[String] and
    (__ \ "order_total_amount").read[Double] and
    (__ \ "order_created_at").read[DateTime]
    )(Order.apply _)
}

