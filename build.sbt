name := "case-ifood"

lazy val commonSettings = Seq(
  version := "0.1",
  scalaVersion := "2.12.8")


lazy val root = (project in file(".")).settings(commonSettings).aggregate(commons, flinkPipeline, dataUtils, sparkPipeline)

lazy val commons = (project in file("commons")).settings(
  commonSettings,
  name := "commons",
  libraryDependencies ++= Seq (
    "com.typesafe.play" %% "play-json" % "2.8.1",
    "com.typesafe.slick" %% "slick" % "3.3.2",
    "com.github.tminglei" %% "slick-pg" % "0.18.0"
  )
)

val flinkVersion = "1.9.3"

lazy val flinkPipeline = (project in file("flink-pipeline")).settings(
  commonSettings,
  name := "flink-pipeline",
  libraryDependencies ++= Seq (
    "org.apache.flink" %% "flink-cep-scala" % flinkVersion,
    "org.apache.flink" %% "flink-streaming-scala" % flinkVersion,
//    "org.apache.flink" % "flink-metrics" % flinkVersion,
//    "org.apache.flink" % "flink-metrics-dropwizard" % flinkVersion,
    "org.apache.flink" %% "flink-connector-kafka" % flinkVersion,

//    "org.apache.flink" % "flink-table" % flinkVersion,
//    "org.apache.flink" %% "flink-table-api-scala" % flinkVersion,
//    "org.apache.flink" %% "flink-table-api-java" % flinkVersion,
//    "org.apache.flink" %% "flink-table-api-scala-bridge" % flinkVersion,
//    "org.apache.flink" %% "flink-table-api-java-bridge" % flinkVersion,
//    "org.apache.flink" % "flink-table-common" % flinkVersion,
//    "org.apache.flink" %% "flink-clients" % flinkVersion,
//    "org.apache.flink" %% "flink-table-planner" % flinkVersion,
//    "org.apache.flink" %% "flink-table-planner-blink" % flinkVersion,

  )
).dependsOn(commons)

lazy val dataUtils = (project in file("data-utils")).settings(
  commonSettings,
  name := "flink-pipeline",
  libraryDependencies ++= Seq (
    "com.typesafe.akka" %% "akka-stream" % "2.6.14",
    "com.lightbend.akka" %% "akka-stream-alpakka-slick" % "2.0.0",
    "org.apache.spark" %% "spark-sql" % sparkVersion,
  )
).dependsOn(commons)

val sparkVersion = "3.1.1"
lazy val sparkPipeline = (project in file("spark-pipeline")).settings(
  commonSettings,
  name := "spark-pipeline",
  libraryDependencies ++= Seq (
    "org.apache.spark" %% "spark-streaming" % sparkVersion,
    "org.apache.spark" %% "spark-sql-kafka-0-10" % sparkVersion,
    "org.apache.spark" %% "spark-sql" % sparkVersion,
    "org.postgresql" % "postgresql" % "42.2.19"

  )
).dependsOn(commons)


//assemblyJarName in assembly := "flink-example.jar"
