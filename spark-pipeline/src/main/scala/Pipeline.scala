import org.apache.spark._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{Dataset, Row, SaveMode, SparkSession}
import org.apache.spark.sql.types.{DoubleType, StringType, StructField, StructType}
import org.apache.log4j.Logger
import org.apache.log4j.Level
import sinks.PostgresSinkConfig
object Pipeline extends App {

  Logger.getLogger("org").setLevel(Level.OFF)

  val spark = SparkSession.builder()
    .master("local")
    .getOrCreate()

  import spark.implicits._

  val schema = StructType(
    List(
      StructField("id", StringType, nullable = false),
      StructField("customer_id", StringType, nullable = false),
      StructField("totalValue", DoubleType, nullable = false),
      StructField("createdAt", StringType, nullable = false),
      ))

  val source = spark.readStream
    .format("kafka")
    .option("kafka.bootstrap.servers", "localhost:9090")
    .option("subscribePattern", "ifood.orders.*")
    .option("auto.offset.reset", "earliest")
    .load()

  val df = source
    .selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)", "CAST(topic AS STRING)")
    .select(from_json($"value", schema).alias("orders"), $"topic")
    .select("orders.*", "topic")
    .withColumn("time", to_date(to_timestamp($"createdAt", "yyyy-MM-dd'T'HH:mm:ss.SSSZ")))
    .withColumn("type", regexp_replace($"topic","ifood.orders.",""))
    .drop("topic")

//  Utils.printDataframe(df)

  val featuresDf = df
    .groupBy(col("customer_id"), window(col("time"), "1 day"), col("type"))
    .agg(count("customer_id").as("n_orders"), sum("totalValue").as("total_amount"))
    .withColumn("day", col("window").getField("end").cast("date"))
    .drop("window")

  Utils.printDataframe(featuresDf, await = false, complete = true)

  featuresDf.writeStream
    .outputMode("complete")
    .foreachBatch{(batch: Dataset[Row], _: Long) =>
      batch
        .write
        .mode(SaveMode.Append)
        .option("driver", PostgresSinkConfig.driver)
        .jdbc(PostgresSinkConfig.url, PostgresSinkConfig.table, PostgresSinkConfig.properties)
    }
    .start()
    .awaitTermination()

}
