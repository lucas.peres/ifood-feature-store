package sinks

import java.util.Properties
import scala.collection.JavaConverters._

object PostgresSinkConfig {

  val options: Map[String, String] = Map(
    "dbtable" -> "ifood.unit_features", // table
    "user" -> "postgres", // Database username
    "password" -> "postgres", // Password
    "driver" -> "org.postgresql.Driver",
    "url" -> "jdbc:postgresql://localhost:5432/postgres",
    "mode" -> "overwrite"
  )

  val properties = new Properties()
  properties.putAll(options.asJava)

  val table: String = options("dbtable")
  val url: String = options("url")
  val driver :String = options("driver")


}
