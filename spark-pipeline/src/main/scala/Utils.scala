import org.apache.spark.sql.DataFrame

object Utils {

  def printDataframe(df: DataFrame, await: Boolean = true, complete: Boolean = false): Unit = {
    val p = df.writeStream
      .format("console")
      .outputMode(if(complete) "complete" else "append")
      .option("truncate", "false")
      .start()

    if(await) p.awaitTermination()
  }

}
