-- connect postgres;

create schema ifood;

create table ifood.orders (
    id character varying not null,
    customer_id character varying not null,
    total_value decimal not null,
    created_at timestamp without time zone not null,
    constraint order_pk primary key (id)
);