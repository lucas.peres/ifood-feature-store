# Case ifood

Este projeto conta com uma implementaçao inicial para o desafio do ifood de [Engenheiro de Dados e ML](https://github.com/ifood/ifood-data-ml-engineer-test), 
onde o objetivo é criar uma ferramenta de *feature store*. Utilizei apenas dois dos arquivos disponibilizados:
o de [Pedidos](https://ifood-data-architect-test-source.s3-sa-east-1.amazonaws.com/order.json.gz) e o de [Status](https://ifood-data-architect-test-source.s3-sa-east-1.amazonaws.com/status.json.gz).

Todos os serviços externos necessários encontram-se na pasta [docker](docker), organizados em um `docker-compose.yml`.

### Preparando para a execução
Antes de executar a solução, é necessário realizar uma carga inicial no PostgreSQL com os dados dos pedidos. 
No módulo `dataUtils`, basta executar o objeto `OrderDataLoad`, modificando o endereço do arquivo json dos pedidos,
para realizar a carga.

Em seguida, para simular a stream de status, é necessário ordenar o seu arquivo em relação à data. 
Embora, em um cenário real, não haja a garantia de que os dados chegarão sempre ordenados,
no arquivo existem status desordenados em diferenças de muitos dias, o que não é algo comum de ser encontrado. 
Para gerar o arquivo ordenado, basta executar, no módulo `dataUtils`, o objeto `StatusDataSorter`.

### Pipeline 1 - detecção de padrões com Flink
O primeiro pipeline é responsável por identificar os pedidos que foram concluídos ou cancelados, 
através dos status emitidos. Quando é identificado um padrão, o payload do pedido é recuperado do PostgreSQL 
e enviado para o Kafka, no tópico referente a sua categoria (`concluded` ou `cancelled`). 

Para executá-lo, basta chamar o objeto `Pipeline` no módulo `flinkPipeline`, modificando o endereço
do arquivo com os status ordenados.

### Pipeline 2 - geração das features com Spark Streaming
O segundo pipeline é responsável por ingerir os dados dos pedidos do Kafka e 
fazer as agregações temporais, informando sua quantidade e o valor total gasto, por dia, por usuário e por tipo. 
Em seguida, os resultados computados são salvos no PostgreSQL.

Para executá-lo, basta chamar o objeto `Pipeline` no módulo `sparkPipeline`.

### Troubleshooting

Como o PostgreSQL roda com as configurações padrões via Docker, algumas vezes em que os pipelines são executados 
podem ocorrer erros devido a exceções provenientes da conexão com o banco. Em alguns casos, devemos executar primeiro o pipeline
Spark e, depois, o do Flink.

Caso queira utilizar uma instância local, basta modificar os parâmetros de conexão com o banco nos arquivos:

* `resources/application.conf` nos módulos `dataUtils` e `flinkPipeline`;
* `sinks.PostgresSinkConfig` no módulo `sparkPipeline`.