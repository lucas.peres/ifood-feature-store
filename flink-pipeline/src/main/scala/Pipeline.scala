import functions.GetOrderFunction
import models.Order
import org.apache.flink.cep.scala.CEP
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import sources.StatusSources
import org.apache.flink.streaming.api.scala._
import patterns._
import sinks.Sinks

object Pipeline extends App {

  implicit val env = StreamExecutionEnvironment.getExecutionEnvironment
  env.setParallelism(1)

  //TODO modificar o endereço do arquivo dos status ordenados
  val statusSource = StatusSources.fromFile("../sorted-status.json")

  val keyedStatusSource = statusSource.keyBy(status => status.order_id)

  val concludedStream = CEP.pattern(keyedStatusSource, OrderConcludedPattern(3))
    .process(StatusPatternProcessFunction)
  concludedStream.print("Concluded")

  val cancelledStream = CEP.pattern(keyedStatusSource, OrderCancelledPattern(3))
    .process(StatusPatternProcessFunction)
  cancelledStream.print("Cancelled")

  val getOrderFunction = new GetOrderFunction

  def getOrderPayload(orderIdStream: DataStream[String]): DataStream[String] = {
    orderIdStream.flatMap(getOrderFunction)
    .map(order => Order.format.writes(order).toString())
  }

  getOrderPayload(concludedStream)
    .addSink(Sinks.getKafkaSink("localhost:9090", "ifood.orders.concluded"))

  getOrderPayload(cancelledStream)
    .addSink(Sinks.getKafkaSink("localhost:9090", "ifood.orders.cancelled"))

  env.execute("feature-storage-pipeline")
}
