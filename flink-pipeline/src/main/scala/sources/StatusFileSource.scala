package sources

import models.Status
import org.apache.flink.streaming.api.functions.source.SourceFunction
import play.api.libs.json.Json

import scala.io.Source

case class StatusFileSource(filePath: String) extends SourceFunction[Status] {
  override def run(ctx: SourceFunction.SourceContext[Status]): Unit = {
    while(true){
      val src = Source.fromFile(filePath)

      src
        .getLines()
        .foreach{ line =>
          val status = Json.parse(line).as[Status]
          ctx.collect(status)
        }

      src.close()
    }
  }

  override def cancel(): Unit = ()
}
