package sources

import java.util.Properties

import models.Status
import org.apache.flink.api.common.serialization.SimpleStringSchema
import org.apache.flink.streaming.api.functions.source.SourceFunction
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer
import play.api.libs.json.Json

object StatusSources {

  def fromFile(filePath: String)
              (implicit env: StreamExecutionEnvironment): DataStream[Status] = {
    env.addSource(StatusFileSource(filePath))
  }

  def fromKafka(broker: String)
               (implicit env: StreamExecutionEnvironment): DataStream[Status] = {
    val properties = new Properties()
    properties.setProperty("bootstrap.servers", broker)
    properties.setProperty("group.id", "consumer")
    env.addSource(
      new FlinkKafkaConsumer("ifood.status", new SimpleStringSchema(), properties)
    ).map(record => Json.parse(record).as[Status])
  }

}
