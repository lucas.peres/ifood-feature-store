package sinks

import java.lang
import java.util.Properties

import org.apache.flink.streaming.connectors.kafka.{FlinkKafkaProducer, KafkaSerializationSchema}
import org.apache.kafka.clients.producer.ProducerRecord

object Sinks {

  private class StringSchema(topic: String) extends KafkaSerializationSchema[String] {
    override def serialize(element: String, timestamp: lang.Long): ProducerRecord[Array[Byte], Array[Byte]] = {
      new ProducerRecord[Array[Byte], Array[Byte]](topic, "".getBytes(), element.getBytes())
    }
  }

  def getKafkaSink(bootstrapServer: String, topic: String): FlinkKafkaProducer[String] = {
    val properties = new Properties
    properties.setProperty("bootstrap.servers", bootstrapServer)

    new FlinkKafkaProducer[String](
      topic,
      new StringSchema(topic),
      properties,
      FlinkKafkaProducer.Semantic.EXACTLY_ONCE)
  }


}
