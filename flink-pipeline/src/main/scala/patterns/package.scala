import java.util

import models.Status
import org.apache.flink.cep.functions.PatternProcessFunction
import org.apache.flink.util.Collector
import scala.collection.JavaConverters._

package object patterns {

  object StatusPatternProcessFunction extends PatternProcessFunction[Status, String] {
    override def processMatch(`match`: util.Map[String, util.List[Status]],
                              ctx: PatternProcessFunction.Context,
                              out: Collector[String]): Unit = {
      val status = `match`.asScala.values.head.asScala.head
      out.collect(status.order_id)
    }
  }

}
