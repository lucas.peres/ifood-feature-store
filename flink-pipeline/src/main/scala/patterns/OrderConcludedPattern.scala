package patterns

import models.Status
import org.apache.flink.cep.scala.pattern.Pattern
import org.apache.flink.streaming.api.windowing.time.Time


object OrderConcludedPattern {

  def apply(withinHours: Long): Pattern[Status, Status] = {
    Pattern
      .begin[Status]("start").where(_.value == Status.REGISTERED)
      .followedBy("concluded").where(_.value == Status.CONCLUDED)
      .within(Time.hours(withinHours))
  }

}
