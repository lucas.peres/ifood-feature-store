package functions

import models.Order
import org.apache.flink.api.common.functions.RichFlatMapFunction
import org.apache.flink.runtime.concurrent.Executors
import org.apache.flink.streaming.api.scala.async.{AsyncFunction, ResultFuture}
import org.apache.flink.util.Collector

import scala.concurrent.{ExecutionContext, Future}
import slick.OrderTable
import slick.lifted.TableQuery
import slick.jdbc.PostgresProfile.api._

class GetOrderFunction() extends RichFlatMapFunction[String, Order]
  with AsyncFunction[String, Order]{

  object OrderRepository extends Serializable {

    val ordersTable = TableQuery[OrderTable]

    val db = Database.forConfig("db.default")

    def getOrder(id: String): Future[Option[Order]] = {
      val action = ordersTable.filter(_.id === id).result.headOption

      db.run(action)
    }

  }

  implicit lazy val executor: ExecutionContext = ExecutionContext.fromExecutor(Executors.directExecutor())

  private def getOrder(id: String) = {
    OrderRepository.getOrder(id)
  }

  override def flatMap(value: String, out: Collector[Order]): Unit = {
    getOrder(value)
      .map{
        case Some(order) => out.collect(order)
        case None => ()
      }
  }

  override def asyncInvoke(input: String, resultFuture: ResultFuture[Order]): Unit = {
    getOrder(input)
      .map{
        case Some(order) => resultFuture.complete(Seq(order))
        case None => resultFuture.complete(Nil)
      }
  }
}
