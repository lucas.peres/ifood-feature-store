import java.nio.file.Paths

import akka.Done
import akka.actor.ActorSystem
import akka.stream.Attributes
import akka.stream.scaladsl.{FileIO, Framing}
import akka.util.ByteString
import models.Order
import play.api.libs.json.Json
import slick.SlickHelper

import scala.concurrent.Future

object OrderDataLoad extends App {

  implicit val system: ActorSystem = ActorSystem()

  import system._
  import Attributes.LogLevels._

  // TODO modificar o endereço do arquivo json de pedidos
  val path = Paths.get("../order.json")

  val result = FileIO.fromPath(path)
    .via(Framing.delimiter(ByteString("\n"), 1e10.toInt, allowTruncation = true))
    .take(2441075) // daqui em diante os ids são repetidos
    .map(_.utf8String)
    .map(Json.parse)
    .map(_.as[Order](Order.reads))
    .log("orders")
    .addAttributes(Attributes.logLevels(onElement = Info, onFailure = Error, onFinish = Info))
    .grouped(10000)
    .runWith(SlickHelper.ordersSink)
    .recoverWith{
      case t: Throwable => t.printStackTrace()
        Future.successful(Done)
    }


  for {
    _ <- result
    _ <- system.terminate()
  } yield sys.exit(0)


}
