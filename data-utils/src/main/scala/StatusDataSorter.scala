import org.apache.spark.sql.functions._
import org.apache.spark.sql._
import org.apache.log4j.Logger
import org.apache.log4j.Level

object StatusDataSorter extends App {
  Logger.getLogger("org").setLevel(Level.OFF)

  val spark: SparkSession = SparkSession.builder()
    .master("local")
    .getOrCreate()

  val df: DataFrame = spark.read.json("../status.json") // localização do arquivo json
  val sorted_df: Dataset[Row] = df.sort(asc("created_at"))

  sorted_df
    .repartition(1) // salva em apenas um arquivo
    .write
    .mode(SaveMode.Append)
    .json("../sorted-status") // diretório onde será salvo o json

}
