package slick

import akka.Done
import akka.stream.alpakka.slick.scaladsl.{Slick, SlickSession}
import akka.stream.scaladsl.Sink
import models.Order
import slick.lifted.TableQuery
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.{ExecutionContext, Future}

object SlickHelper {

  implicit val session: SlickSession = SlickSession.forConfig("slick-postgres")

  val ordersTable = TableQuery[OrderTable]

  private def insertOrders(orders: Seq[Order])
                          (implicit ec: ExecutionContext): DBIO[Int] = (ordersTable ++= orders).map(_.get)

  def ordersSink(implicit ec: ExecutionContext): Sink[Seq[Order], Future[Done]] = Slick.sink[Seq[Order]]{ o: Seq[Order] => insertOrders(o)(ec)}
}
